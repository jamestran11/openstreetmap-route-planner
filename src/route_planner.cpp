#include "route_planner.h"
#include <algorithm>

RoutePlanner::RoutePlanner(RouteModel &model, float start_x, float start_y, float end_x, float end_y): m_Model(model) {
    // Convert inputs to percentage:
    start_x *= 0.01;
    start_y *= 0.01;
    end_x *= 0.01;
    end_y *= 0.01;

    start_node = &m_Model.FindClosestNode(start_x, start_y);
    end_node = &m_Model.FindClosestNode(end_x, end_y);
    
}

float RoutePlanner::CalculateHValue(RouteModel::Node const *node) {
    return end_node->distance(*node);
}

void RoutePlanner::AddNeighbors(RouteModel::Node *current_node) {
    current_node->FindNeighbors();
    for(auto n : current_node->neighbors) {
        n->parent = current_node;
        n->h_value = CalculateHValue(n);
        n->g_value = n->distance(*n->parent) + current_node->g_value;
        open_list.emplace_back(n);
        n->visited=true;
    }

}

bool compare(RouteModel::Node* node1, RouteModel::Node* node2) {
    return node1->g_value + node1->h_value < node2->g_value + node2->h_value;
}

RouteModel::Node *RoutePlanner::NextNode() {
    std::sort(open_list.begin(), open_list.end(), compare);
    auto nextNode = open_list.front();
    open_list.erase(open_list.begin());
    return nextNode;
}

std::vector<RouteModel::Node> RoutePlanner::ConstructFinalPath(RouteModel::Node *current_node) {
    // Create path_found vector
    distance = 0.0f;
    std::vector<RouteModel::Node> path_found;

    while(current_node->parent != nullptr) {
        path_found.push_back(*current_node);
        distance += current_node->distance(*current_node->parent);
        current_node = current_node->parent;
    }
    path_found.push_back(*current_node);

    distance *= m_Model.MetricScale(); // Multiply the distance by the scale of the map to get meters.
    std::reverse(path_found.begin(), path_found.end());
    return path_found;

}

void RoutePlanner::AStarSearch() {
    RouteModel::Node *current_node = nullptr;

    current_node = start_node;
    start_node->visited=true; //why need this? without it, it runs forever
    AddNeighbors(current_node);
    while(current_node != end_node) {
        current_node = NextNode();
        AddNeighbors(current_node);
    }

    m_Model.path = ConstructFinalPath(current_node);



}

